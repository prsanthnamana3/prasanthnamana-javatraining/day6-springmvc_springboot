package com.prasanth.training.SpringBootApp.service;

import com.prasanth.training.SpringBootApp.pojos.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonSerImpl implements PersonService{
    Map<Integer, Person> personMap = new HashMap<>();

    @Override
    public List<Person> getAllPersons() {
        List<Person> allPersons = new ArrayList<>();
        personMap.forEach((k, v) -> allPersons.add(v));
        return allPersons;
    }

    @Override
    public Person getAllPersonById(int id) {
        return personMap.get(id);
    }

    @Override
    public String deletePersonById(int id) {
        if(personMap.get(id)!= null){
            personMap.remove(id);
            return "Person with id "+ id+" deleted";
        }
        return "Person does not exists";
    }

    @Override
    public String updatePerson(Person person) {
        if(personMap.get(person.getId())!= null){
            personMap.put(person.getId(),person);
            return "Person with id "+ person.getId()+" updated successfully";
        }
        return "Person does not exists";
    }

    @Override
    public String addPerson(Person person) {
        personMap.put(person.getId(), person);
        return "Added successfully";
    }
}
