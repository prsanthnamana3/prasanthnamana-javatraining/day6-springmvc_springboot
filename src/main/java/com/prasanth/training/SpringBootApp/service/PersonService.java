package com.prasanth.training.SpringBootApp.service;

import com.prasanth.training.SpringBootApp.pojos.Person;

import java.util.List;

public interface PersonService {
    List<Person> getAllPersons();

    Person getAllPersonById(int id);

    String deletePersonById(int id);

    String updatePerson(Person person);

    String addPerson(Person person);
}
